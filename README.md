# Test task results

## What was done?
- The codebase covers mentioned in the task requirements
- Currency icons and descriptions were provided 
- Per-second request to the backend without interruption by user actions is implemented
- One-second timeout on backend answer was added
- Smooth animation was implemented with `Diff Utils` tool is added
- Added `setHasFixedSize(true)` optimization for RecyclerView items 


## Further improvements

### Codebase
- Add `WorkManager` implementation for handling Network State
- Use ServiceResult models and RepositoryResult models that are presented in the codebase under `RepositoryResult.kt`
- Add Gradle improvements: separate library versions from dependencies, optimization configurations


### Test coverage
- Add instrumentation tests,  with `Espresso` 
- Add unit tests for DI classes

### Environment improvements
- Enable git hooks on the pre-commit step. Run unit tests for the changed classes, add check to commit message, etc.
- Add SonarQube cheks
- Prepare CI/DI environment. Move release related data from Gradle to a build machine.
- Update lint and ProGuard configuration file according to business, libraries and dev-team requirements

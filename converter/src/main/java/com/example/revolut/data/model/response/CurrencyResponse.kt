package com.example.revolut.data.model.response

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal
import java.util.Date

//Test data
//{"base":"EUR","date":"2018-09-06","rates":{"AUD":1.6203,"BGN":1.9605,"BRL":4.8034,"CAD":1.5375,"CHF":1.1302,"CNY":7.9643,"CZK":25.777,"DKK":7.4747,"GBP":0.90041,"HKD":9.1544,"HRK":7.452,"HUF":327.28,"IDR":17365.0,"ILS":4.1807,"INR":83.92,"ISK":128.11,"JPY":129.86,"KRW":1307.9,"MXN":22.419,"MYR":4.8236,"NOK":9.7996,"NZD":1.7676,"PHP":62.743,"PLN":4.3287,"RON":4.6497,"RUB":79.767,"SEK":10.616,"SGD":1.6039,"THB":38.222,"TRY":7.6466,"USD":1.1662,"ZAR":17.866}}

data class CurrencyResponse(
    val base: String = "EUR",
    val date: String = Date().toString(),
    @SerializedName("rates") val rateList: Map<String, BigDecimal> = mutableMapOf()
)


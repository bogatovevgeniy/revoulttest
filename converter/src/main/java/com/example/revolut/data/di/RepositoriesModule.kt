package com.example.revolut.data.di

import com.example.revolut.data.exchange.ExchangeEndpoints
import com.example.revolut.data.exchange.ExchangeRepositoryImpl
import com.example.revolut.domain.ExchangeRepository
import dagger.Module
import dagger.Provides

@Module(includes = arrayOf(NetworkModule::class))
class RepositoriesModule {

    @Provides
    fun providesCurrencyRepository(exchangeEndpoints: ExchangeEndpoints): ExchangeRepository =
        ExchangeRepositoryImpl(exchangeEndpoints)

}
package com.example.revolut.data.exchange

import com.example.revolut.data.model.response.CurrencyResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ExchangeEndpoints {

    @GET("api/android/latest")
    fun getCurrentExchangeRates(@Query("base") currency: String): Single<CurrencyResponse>
}

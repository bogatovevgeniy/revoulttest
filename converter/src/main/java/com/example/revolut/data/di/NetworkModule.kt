package com.example.revolut.data.di

import com.example.revolut.data.exchange.ExchangeEndpoints
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule {

    // TODO Add provision on baseURL in a dynamic way
    @Provides
    fun providesRetrofitClient(): Retrofit =
        Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://hiring.revolut.codes/")
            .build()

    @Provides
    fun providesExchangeService(retrofit: Retrofit) =
        retrofit.create(ExchangeEndpoints::class.java)

}
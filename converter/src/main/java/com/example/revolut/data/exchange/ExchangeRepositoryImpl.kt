package com.example.revolut.data.exchange

import com.example.revolut.data.model.response.CurrencyResponse
import com.example.revolut.domain.ExchangeRepository
import com.example.revolut.utils.formatBigDecimal
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal
import java.util.Currency
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * The repository implemented with an assumption that there is no necessary to store data in ORM.

 *
 * Behaviour:
 *
 */
class ExchangeRepositoryImpl @Inject constructor(
    val exchangeEndpoints: ExchangeEndpoints
) : ExchangeRepository {

    private lateinit var lastKnownCoefficients: MutableMap<Currency, BigDecimal>

    override fun getExchangeRates(currency: Currency, amount: BigDecimal) = getApiExchangeProposal(currency)

    /**
     * Returns observable list of ExchangeProposals multiplied on exchange amount
     */
    private fun getApiExchangeProposal(currency: Currency): Single<Map<Currency, BigDecimal>> {
        return exchangeEndpoints.getCurrentExchangeRates(currency.currencyCode)
            .map { convertToExchangeList(it) }
            .timeout(1, TimeUnit.SECONDS)
            .onErrorReturn { lastKnownCoefficients }
            .subscribeOn(Schedulers.io())
    }

    private fun convertToExchangeList(
        currencyResponse: CurrencyResponse
    ): Map<Currency, BigDecimal> {

        val mapResult: MutableMap<Currency, BigDecimal> = mutableMapOf()
        currencyResponse.rateList.entries.forEach {
            mapResult[Currency.getInstance(it.key)] = it.value.formatBigDecimal()
        }

        lastKnownCoefficients = mapResult
        return mapResult
    }
}


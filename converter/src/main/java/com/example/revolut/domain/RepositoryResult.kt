package com.example.revolut.domain

import retrofit2.HttpException

sealed class ServiceResult<ApiPayload> {
    data class Success<ApiPayload>(val data: ApiPayload? = null) : ServiceResult<ApiPayload>()
    data class Error<ApiPayload>(val error: HttpException) : ServiceResult<ApiPayload>()
}

/**
 * #Data - cached or loaded data
 * #TypedError - some set of specific method errors that can be thrown only by a specific method
 * #T - used here as a mark of a stub Type
 */
sealed class RepositoryResult<Data, TypedError> {
    data class Loading<Data, TypedError>(val data: Data? = null, val primary: Boolean = true) : RepositoryResult<Data, TypedError>()
    data class Success<Data, TypedError>(val data: Data? = null) : RepositoryResult<Data, TypedError>()
    data class TypedError<Data, TypedError>(val data: TypedError? = null, val error: Exception? = null) : RepositoryResult<Data, TypedError>()
    data class Error<Data, TypedError>(val error: Exception? = null) : RepositoryResult<Data, TypedError>()
}
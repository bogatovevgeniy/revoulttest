package com.example.revolut.domain.di

import com.example.revolut.domain.ExchangeRepository
import com.example.revolut.domain.RealTimeExchangeInteractor
import dagger.Module
import dagger.Provides

@Module
public class ExchangeModule {

    @Provides
    fun provideExchangeService(
        exchangeRepository: ExchangeRepository
    ): RealTimeExchangeInteractor =
        RealTimeExchangeInteractor(exchangeRepository)
}
package com.example.revolut.domain.model

import java.math.BigDecimal
import java.util.Currency

/**
 * The class contains set of Presentation level models
 * A single exchange rate per one currency
 */
interface ExchangeModel

data class ExchangeProposal(val currency: Currency, var exchangeOutcome: BigDecimal, val isBase: Boolean = false): ExchangeModel

class EmptyProposals: ExchangeModel{
    override fun equals(other: Any?): Boolean {
        return true
    }

    override fun hashCode(): Int {
        return 0
    }
}

fun List<ExchangeProposal>?.getBaseCurrency(): Currency? {
    if (this == null) return null

    var result: ExchangeProposal? = null
    val iterator = iterator()
    while (iterator.hasNext()) {
        val next = iterator.next()
        if (next.isBase) {
            result = next
            break
        }
    }
    return result?.currency ?: throw IllegalStateException()
}

fun List<ExchangeProposal>?.getBaseProposal(): ExchangeProposal? {
    if (this == null)  return null

    var result: ExchangeProposal? = null
    val iterator = iterator()
    while (iterator.hasNext()) {
        val next = iterator.next()
        if (next.isBase) {
            result = next
            break
        }
    }
    return result ?: throw IllegalStateException()
}
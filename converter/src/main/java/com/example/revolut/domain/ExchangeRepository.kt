package com.example.revolut.domain

import io.reactivex.Single
import java.math.BigDecimal
import java.util.Currency

interface ExchangeRepository {
    fun getExchangeRates(currency: Currency, amount: BigDecimal): Single<Map<Currency, BigDecimal>>
}
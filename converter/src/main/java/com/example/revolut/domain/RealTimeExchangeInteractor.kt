package com.example.revolut.domain

import androidx.annotation.VisibleForTesting
import com.example.revolut.domain.model.ExchangeProposal
import com.example.revolut.domain.model.getBaseCurrency
import com.example.revolut.domain.model.getBaseProposal
import com.example.revolut.utils.formatBigDecimal
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal
import java.util.Currency


class RealTimeExchangeInteractor(private val exchangeRepository: ExchangeRepository) :
    ExchangeInteractor {

    @VisibleForTesting
    public var exchangeProposals: MutableList<ExchangeProposal> = mutableListOf()
    private var lastKnownMultipliers: Map<Currency, BigDecimal> = mapOf()

    override fun getExchangeProposals(currency: Currency, amount: BigDecimal): Single<List<ExchangeProposal>> {
        return if (exchangeProposals.isNotEmpty()
            && currency == exchangeProposals[0].currency
            && amount != exchangeProposals[0].exchangeOutcome
        ) {
            Single.just(fastRecalculationNewAmount(currency, amount, exchangeProposals))
        } else {
            exchangeRepository.getExchangeRates(currency, amount)
                .map { mapMultipliersMapToProposalsList(currency, amount, it, exchangeProposals) }
                .subscribeOn(Schedulers.io())

        }
    }


    private fun mapMultipliersMapToProposalsList(
        currency: Currency,
        amount: BigDecimal,
        coeficientMap: Map<Currency, BigDecimal>,
        previousProposals: List<ExchangeProposal>
    ): List<ExchangeProposal> {
        if (coeficientMap.isEmpty()) return mutableListOf()

        val resultList = mutableListOf<ExchangeProposal>()
        var calculatedProposals: List<ExchangeProposal>?

        resultList.add(ExchangeProposal(currency, amount, true))

        calculatedProposals = if (exchangeProposals.isEmpty()) {
            initialCalculation(amount, coeficientMap)
        } else {
            calculateWithSavingCurrenciesOrder(currency, amount, coeficientMap, previousProposals)
        }

        resultList.addAll(calculatedProposals)
        exchangeProposals = resultList
        lastKnownMultipliers = coeficientMap
        return resultList
    }

    private fun initialCalculation(amount: BigDecimal, coeficientMap: Map<Currency, BigDecimal>): List<ExchangeProposal> {
        val resultList = mutableListOf<ExchangeProposal>()
        coeficientMap.entries.forEach {
            resultList.add(ExchangeProposal(it.key, (it.value * amount).formatBigDecimal()))
        }
        return resultList
    }

    private fun calculateWithSavingCurrenciesOrder(
        currency: Currency,
        amount: BigDecimal,
        coeficientMap: Map<Currency, BigDecimal>,
        previousProposals: List<ExchangeProposal>
    ): List<ExchangeProposal> {
        val resultList = mutableListOf<ExchangeProposal>()
        val previousBase = previousProposals[0]
        val previousCurrency = previousProposals.getBaseCurrency()

        if (previousProposals.getBaseCurrency() != currency) {
            resultList.add(ExchangeProposal(previousBase.currency, previousBase.exchangeOutcome, false))
        }
        previousProposals.forEach { exchangeProposal ->
            if (currency != exchangeProposal.currency && exchangeProposal.currency != previousCurrency) {
                if (amountIsZeroValue(amount)) {
                    resultList.add(ExchangeProposal(exchangeProposal.currency, 0.formatBigDecimal()))
                } else {
                    val updatedValue = (amount * coeficientMap.getValue(exchangeProposal.currency)).formatBigDecimal()
                    resultList.add(ExchangeProposal(exchangeProposal.currency, updatedValue))
                }
            }
        }
        return resultList
    }

    private fun fastRecalculationNewAmount(
        currency: Currency,
        amount: BigDecimal,
        previousProposals: MutableList<ExchangeProposal>
    ): List<ExchangeProposal> {
        val resultList = mutableListOf<ExchangeProposal>()
        resultList.add(ExchangeProposal(currency, amount, true))
        if (amountIsZeroValue(amount)) {
            fillWithZeros(currency, previousProposals, resultList)
        } else if (previousBaseValueIsNull(previousProposals)) {
            if (lastKnownMultipliers.isEmpty()) {
                fillWithZeros(currency, previousProposals, resultList)
            } else {
                previousProposals.forEach { exchangeProposal ->
                    if (currency != exchangeProposal.currency) {
                        val multiplier = lastKnownMultipliers.getValue(exchangeProposal.currency)
                        val updatedValue = (amount * multiplier).formatBigDecimal()
                        resultList.add(ExchangeProposal(exchangeProposal.currency, updatedValue))
                    }
                }
            }
        } else {
            val baseProposal = checkNotNull(previousProposals.getBaseProposal())
            val multiplier = amount / baseProposal.exchangeOutcome
            previousProposals.forEach { exchangeProposal ->
                if (currency != exchangeProposal.currency) {
                    val updatedValue = (exchangeProposal.exchangeOutcome * multiplier).formatBigDecimal()
                    resultList.add(ExchangeProposal(exchangeProposal.currency, updatedValue))
                }
            }
        }
        exchangeProposals = resultList
        return resultList
    }

    private fun fillWithZeros(
        currency: Currency,
        previousProposals: MutableList<ExchangeProposal>,
        resultList: MutableList<ExchangeProposal>
    ) {
        previousProposals.forEach { exchangeProposal ->
            if (currency != exchangeProposal.currency) {
                resultList.add(ExchangeProposal(exchangeProposal.currency, 0.formatBigDecimal()))
            }
        }
    }

    private fun amountIsZeroValue(amount: BigDecimal) = 0.formatBigDecimal() == amount

    private fun previousBaseValueIsNull(previousProposals: MutableList<ExchangeProposal>) =
        0.formatBigDecimal() == previousProposals.getBaseProposal()?.exchangeOutcome
}
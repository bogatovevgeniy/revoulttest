package com.example.revolut.domain

import com.example.revolut.domain.model.ExchangeProposal
import io.reactivex.Single
import java.math.BigDecimal
import java.util.Currency

interface ExchangeInteractor {
    fun getExchangeProposals(currency: Currency, amount: BigDecimal): Single<List<ExchangeProposal>>
}

package com.example.revolut.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.revolut.domain.RealTimeExchangeInteractor
import com.example.revolut.domain.model.ExchangeProposal
import com.example.revolut.utils.formatBigDecimal
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.math.BigDecimal
import java.util.Currency
import java.util.concurrent.TimeUnit

class ConverterViewModel(private val interactor: RealTimeExchangeInteractor) : ViewModel() {

    val exchangeProposal: LiveData<List<ExchangeProposal>>
        get() = _exchangeProposal
    private val _exchangeProposal: MutableLiveData<List<ExchangeProposal>> = MutableLiveData()
    val currencyAmountSubject = BehaviorSubject.create<Pair<Currency, BigDecimal>>()
    val disposables = CompositeDisposable()

    init {
        currencyAmountSubject.onNext(Pair(Currency.getInstance("EUR"), 1.formatBigDecimal()))
    }

    fun requestExchangeProposals() {

        disposables.add(
            currencyAmountSubject
                .map { interactor.getExchangeProposals(it.first, it.second).blockingGet() }
                .mergeWith(
                    Observable.interval(1, TimeUnit.SECONDS)
                        .map {
                            interactor.getExchangeProposals(currencyAmountSubject.value!!.first, currencyAmountSubject.value!!.second)
                                .blockingGet()
                        }
                )
                .debounce(100, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<List<ExchangeProposal>>() {

                    override fun onError(e: Throwable) {
                        _exchangeProposal.value = emptyList()
                    }

                    override fun onComplete() {
                    }

                    override fun onNext(proposalList: List<ExchangeProposal>) {
                        _exchangeProposal.value = proposalList
                    }
                })
        )
    }

    override fun onCleared() {
        disposables.dispose()
        super.onCleared()
    }
}

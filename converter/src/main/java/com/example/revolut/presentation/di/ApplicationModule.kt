package com.example.revolut.presentation.di

import android.app.Application
import com.example.revolut.presentation.RevolutApplication
import dagger.Binds
import dagger.Module

@Module
abstract class ApplicationModule {

    @Binds
    abstract fun bindApplication(app: RevolutApplication): Application
}
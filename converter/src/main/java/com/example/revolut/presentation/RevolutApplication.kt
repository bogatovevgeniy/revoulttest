package com.example.revolut.presentation

import com.example.revolut.BuildConfig
import com.example.revolut.presentation.di.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.DispatchingAndroidInjector
import timber.log.Timber
import javax.inject.Inject


class RevolutApplication @Inject constructor() : DaggerApplication() {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>


    val component by lazy {
        DaggerApplicationComponent.builder()
            .application(this)
            .context(this)
            .create(this)
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    override fun applicationInjector(): AndroidInjector<out RevolutApplication> = component

    override fun androidInjector(): AndroidInjector<Any> = androidInjector
}

package com.example.revolut.presentation.di

import android.content.Context
import com.example.revolut.data.di.NetworkModule
import com.example.revolut.data.di.RepositoriesModule
//import com.example.revolut.data.di.RepositoriesModule
import com.example.revolut.domain.di.ExchangeModule
import com.example.revolut.presentation.ConverterAdapter
import com.example.revolut.presentation.RevolutApplication
import com.example.revolut.presentation.di.module.app.ViewModelsBaseModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(
    modules = arrayOf(
        ApplicationModule::class,
        AndroidSupportInjectionModule::class,
        ViewModelsBaseModule::class,
        ExchangeModule::class,
        RepositoriesModule::class,
        NetworkModule::class
    )
)
@Singleton
interface ApplicationComponent : AndroidInjector<RevolutApplication> {

    fun addInjection(adapter: ConverterAdapter)

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<RevolutApplication>() {

        abstract override fun build(): ApplicationComponent

        @BindsInstance
        abstract fun  application(app: RevolutApplication): Builder

        @BindsInstance
        abstract fun context(context: Context): Builder

    }
}
package com.example.revolut.presentation

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.example.revolut.databinding.BaseCurrencyItemBinding
import com.example.revolut.databinding.EmptyItemBinding
import com.example.revolut.databinding.ExchangeProposalItemBinding
import javax.inject.Inject

class ConverterViewHoldersFactory @Inject constructor() {

    fun getConverterViewHolder(
        context: Context,
        parent: ViewGroup,
        viewType: Int
    ): ConverterViewHolder<out ViewDataBinding> {
        return when (viewType) {
            BaseCurrencyItemViewHolder.VIEW_HOLDER_TYPE -> {
                val binding = DataBindingUtil.inflate<BaseCurrencyItemBinding>(
                    LayoutInflater.from(context),
                    viewType,
                    parent,
                    false
                )
                BaseCurrencyItemViewHolder(binding)
            }

            ExchangeProposalItemViewHolder.VIEW_HOLDER_TYPE -> {
                val binding = DataBindingUtil.inflate<ExchangeProposalItemBinding>(
                    LayoutInflater.from(context),
                    viewType,
                    parent,
                    false
                )
                ExchangeProposalItemViewHolder(binding)
            }
            else -> {
                val binding = DataBindingUtil.inflate<EmptyItemBinding>(
                    LayoutInflater.from(context),
                    viewType,
                    parent,
                    false
                )
                EmptyResults(binding)
            }
        }
    }
}
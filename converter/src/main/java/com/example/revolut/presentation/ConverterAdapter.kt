package com.example.revolut.presentation

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.TextWatcher
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.BatchingListUpdateCallback
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListUpdateCallback
import androidx.recyclerview.widget.RecyclerView
import com.example.revolut.R
import com.example.revolut.databinding.BaseCurrencyItemBinding
import com.example.revolut.domain.model.ExchangeProposal
import com.example.revolut.utils.formatBigDecimal
import java.math.BigDecimal
import java.util.Currency

class ConverterAdapter(val context: Context) :
    RecyclerView.Adapter<ConverterViewHolder<out ViewDataBinding>>() {

    val baseCurrencyValue: LiveData<Pair<Currency, BigDecimal>>
        get() = _baseCurrencyLiveData

    private var viewHoldersFactory: ConverterViewHoldersFactory = ConverterViewHoldersFactory()

    private val convertCallback = BatchingListUpdateCallback(ConverterCallback(this))

    var rates: List<ExchangeProposal> = mutableListOf()
        set(newRates) {
            val calculateDiff = DiffUtil.calculateDiff(
                ConverterDiffUtilsCallback(
                    oldList = rates,
                    newList = newRates
                )
            )
            field = newRates
            calculateDiff.dispatchUpdatesTo(convertCallback)
        }

    private val _baseCurrencyLiveData: MutableLiveData<Pair<Currency, BigDecimal>> = MutableLiveData()

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            BaseCurrencyItemViewHolder.VIEW_HOLDER_TYPE
        } else {
            ExchangeProposalItemViewHolder.VIEW_HOLDER_TYPE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConverterViewHolder<out ViewDataBinding> {
        val convertItemViewHolder = viewHoldersFactory.getConverterViewHolder(context, parent, viewType)

        when (convertItemViewHolder) {
            is BaseCurrencyItemViewHolder -> initWatchTextListener(convertItemViewHolder.binding)
            is ExchangeProposalItemViewHolder -> initOnItemClickListener(convertItemViewHolder)
        }

        return convertItemViewHolder
    }

    private fun initWatchTextListener(binding: BaseCurrencyItemBinding) {

        binding.baseCurrencyAmountToExchangeOutcome.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(changedValue: CharSequence?, start: Int, before: Int, count: Int) {
                changedValue?.let {
                    var exchangeResult = 0.formatBigDecimal()
                    if (changedValue.isNotEmpty()) {
                        exchangeResult = changedValue.toString().formatBigDecimal()
                    }

                    _baseCurrencyLiveData.value = Pair(this@ConverterAdapter.rates[0].currency, exchangeResult)
                }
            }
        })
    }

    private fun initOnItemClickListener(holder: ExchangeProposalItemViewHolder) {
        holder.itemView.setOnClickListener {
            val exchangeProposal: ExchangeProposal = holder.itemView.tag as ExchangeProposal
            _baseCurrencyLiveData.value =
                Pair(exchangeProposal.currency, exchangeProposal.exchangeOutcome)
        }
    }

    override fun getItemCount(): Int = rates.size

    override fun onBindViewHolder(holder: ConverterViewHolder<out ViewDataBinding>, position: Int) {
        val exchangeProposal = rates[position]
        val currencyDrawable = getCurrencyDrawable(holder, exchangeProposal)
        holder.bind(exchangeProposal, currencyDrawable)
        holder.binding.root.tag = exchangeProposal
    }

    private fun getCurrencyDrawable(holder: ConverterViewHolder<out ViewDataBinding>, exchangeProposal: ExchangeProposal): Drawable {
        val resources = holder.itemView.context.resources
        val resourceId = resources.getIdentifier(
            exchangeProposal.currency.currencyCode.toLowerCase(),
            "drawable",
            context.getPackageName()
        )
        val currencyDrawable = resources.getDrawable(resourceId, context.theme) ?: resources.getDrawable(R.drawable.aud, context.theme)
        return currencyDrawable
    }

    inner class ConverterDiffUtilsCallback(val oldList: List<ExchangeProposal>, val newList: List<ExchangeProposal>) : DiffUtil.Callback() {
        override fun getNewListSize() = newList.size

        override fun getOldListSize() = oldList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition].currency == newList[newItemPosition].currency

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList[newItemPosition]
        }

        override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
            if (oldList[oldItemPosition].exchangeOutcome != newList[newItemPosition].exchangeOutcome) {
                return newList[newItemPosition].exchangeOutcome
            }
            return null
        }
    }

    inner class ConverterCallback(val adapter: ConverterAdapter) : ListUpdateCallback {
        override fun onChanged(position: Int, count: Int, payload: Any?) {
            adapter.notifyItemRangeChanged(position, count, payload)
        }

        override fun onInserted(position: Int, count: Int) {
            adapter.notifyItemRangeInserted(position, count)
        }

        override fun onMoved(fromPosition: Int, toPosition: Int) {
            adapter.notifyItemMoved(fromPosition, toPosition)
        }

        override fun onRemoved(position: Int, count: Int) {
            adapter.notifyItemRangeRemoved(position, count)
        }
    }
}

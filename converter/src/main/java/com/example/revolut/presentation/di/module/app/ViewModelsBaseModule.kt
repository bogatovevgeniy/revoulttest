package com.example.revolut.presentation.di.module.app

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.revolut.presentation.base.AppViewModelFactory
import com.example.revolut.presentation.di.module.converter.ConverterModule
import dagger.Module
import dagger.Provides
import javax.inject.Provider

@Module(includes = arrayOf(ConverterModule::class))
class ViewModelsBaseModule {

    @Provides
    fun provideViewModelFactory(
        providers: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>
    ): ViewModelProvider.Factory =
        AppViewModelFactory(providers)
}
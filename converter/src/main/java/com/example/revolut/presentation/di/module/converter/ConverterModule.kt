package com.example.revolut.presentation.di.module.converter

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.revolut.domain.RealTimeExchangeInteractor
import com.example.revolut.presentation.ConverterActivity
import com.example.revolut.presentation.ConverterViewModel
import com.example.revolut.presentation.di.ViewModelKey
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module(includes = arrayOf(ConverterModule.ConverterDepsModule::class))
abstract class ConverterModule {

    @ContributesAndroidInjector(
        modules = [
            InjectViewModel::class
        ]
    )
    abstract fun bind(): ConverterActivity

    @Module
    class ConverterDepsModule {

        @Provides
        @IntoMap
        @ViewModelKey(ConverterViewModel::class)
        fun provideConverterViewModel(service: RealTimeExchangeInteractor): ViewModel =
            ConverterViewModel(service)
    }

    @Module
    class InjectViewModel {

        @Provides
        fun provideConverterViewModel(
            factory: ViewModelProvider.Factory,
            target: ConverterActivity

        ): ConverterViewModel =
            ViewModelProvider(target, factory).get(ConverterViewModel::class.java)
    }
}

package com.example.revolut.presentation

import android.os.Bundle
import android.view.KeyEvent
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_DRAGGING
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE
import com.example.revolut.R
import dagger.android.AndroidInjection
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.converter_layout.*
import javax.inject.Inject

class ConverterActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModel: ConverterViewModel
    private var converterAdapter = ConverterAdapter(this)

    private val adapterDataObserver: RecyclerView.AdapterDataObserver
        get() {
            return object : RecyclerView.AdapterDataObserver() {
                override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
                    super.onItemRangeMoved(fromPosition, toPosition, itemCount)
                    converter_list.scrollToPosition(0)
                }
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.converter_layout)
        initUI()
        subscribeOnUserRateChanges()
        subscribeOnExchangeProposals()
    }

    private fun initUI() {
        converter_list.apply {
            adapter = converterAdapter
            setHasFixedSize(true)
            adapter?.registerAdapterDataObserver(adapterDataObserver)

        }
    }

    private fun subscribeOnUserRateChanges() {
        converterAdapter.baseCurrencyValue.observe(this,
            Observer {
                viewModel.currencyAmountSubject.onNext(it)
            })
    }

    private fun subscribeOnExchangeProposals() {
        viewModel.exchangeProposal.observe(this,
            Observer {
                if (!converter_list.isComputingLayout
                    && (converter_list.scrollState == SCROLL_STATE_IDLE
                            || converter_list.scrollState == SCROLL_STATE_DRAGGING)
                ) {
                    converterAdapter.rates = it
                }
            })
    }

    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        return when (event.keyCode) {
            KeyEvent.KEYCODE_ENTER,
            KeyEvent.KEYCODE_COMMA,
            KeyEvent.KEYCODE_PERIOD -> false
            else -> super.dispatchKeyEvent(event)
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.requestExchangeProposals()
    }

    override fun onDestroy() {
        super.onDestroy()
        converterAdapter.unregisterAdapterDataObserver(adapterDataObserver)
    }
}


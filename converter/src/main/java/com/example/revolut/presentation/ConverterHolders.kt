package com.example.revolut.presentation

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat.getSystemService
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.revolut.R
import com.example.revolut.databinding.BaseCurrencyItemBinding
import com.example.revolut.databinding.EmptyItemBinding
import com.example.revolut.databinding.ExchangeProposalItemBinding
import com.example.revolut.domain.model.ExchangeProposal
import com.example.revolut.utils.executeIfNew
import com.example.revolut.utils.updateIfNew
import java.math.RoundingMode


abstract class ConverterViewHolder<Binding : ViewDataBinding>(val binding: Binding) :
    RecyclerView.ViewHolder(binding.root) {

    abstract fun bind(exchangeProposal: ExchangeProposal, currencyImageId: Drawable)
}

class ExchangeProposalItemViewHolder(binding: ExchangeProposalItemBinding) :
    ConverterViewHolder<ExchangeProposalItemBinding>(binding) {

    override fun bind(exchangeProposal: ExchangeProposal, currencyImageId: Drawable) {
        binding.currencyItemTitle.updateIfNew(exchangeProposal.currency.currencyCode)
        binding.currencyItemDescription.text = exchangeProposal.currency.displayName
        binding.currencyItemPic.setImageDrawable(currencyImageId)
        binding.currencyItemExchangeOutcome.updateIfNew(exchangeProposal.exchangeOutcome.toString())
        binding.currencyItemExchangeOutcome.setOnClickListener { v -> binding.root.callOnClick() }
    }

    companion object {
        const val VIEW_HOLDER_TYPE = R.layout.item_exchange_proposal
    }
}

class BaseCurrencyItemViewHolder(binding: BaseCurrencyItemBinding) :
    ConverterViewHolder<BaseCurrencyItemBinding>(binding) {

    override fun bind(exchangeProposal: ExchangeProposal, currencyImageId: Drawable) {
        binding.baseCurrencyItemTitle.updateIfNew(exchangeProposal.currency.currencyCode)
        binding.baseCurrencyItemDescription.text = exchangeProposal.currency.displayName
        binding.baseCurrencyItemPic.setImageDrawable(currencyImageId)
        if (!binding.baseCurrencyAmountToExchangeOutcome.isFocused) {
            binding.baseCurrencyAmountToExchangeOutcome.executeIfNew {
                text = exchangeProposal.exchangeOutcome.setScale(0, RoundingMode.UP).toString()
                val imm: InputMethodManager? = getSystemService(context, InputMethodManager::class.java)
                imm?.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
                this.requestFocus()
                if (text.isNotEmpty()) {
                    binding.baseCurrencyAmountToExchangeOutcome.setSelection(text.length)
                }
            }
        }
    }

    companion object {
        const val VIEW_HOLDER_TYPE = R.layout.item_base_currency
    }


}

class EmptyResults(binding: EmptyItemBinding) :
    ConverterViewHolder<EmptyItemBinding>(binding) {

    override fun bind(exchangeProposal: ExchangeProposal, currencyImageId: Drawable) {
    }

    companion object {
        const val VIEW_HOLDER_TYPE = R.layout.item_base_currency
    }
}
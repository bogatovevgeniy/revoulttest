package com.example.revolut.utils

import android.widget.EditText
import android.widget.TextView

fun TextView.text(text:String) = this.setText(text)

fun EditText.text(text:String) = this.setText(text)

fun EditText.text(float: Float) = this.setText(float.toString())

fun TextView.updateIfNew(text: String) {
    if (this.text.toString() != text) {
        this.setText(text)
    }
}

fun TextView.executeIfNew(block: TextView.() -> Unit) {
    if (this.text.toString() != text) {
        block()
    }
}
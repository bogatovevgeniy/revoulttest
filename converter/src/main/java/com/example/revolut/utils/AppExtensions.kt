package com.example.revolut.utils

import android.app.Activity
import android.content.Context
import com.example.revolut.presentation.RevolutApplication
import java.math.BigDecimal
import java.math.RoundingMode

val Activity.app: RevolutApplication
    get() = application as RevolutApplication

val Context.app: RevolutApplication
    get() = applicationContext as RevolutApplication

fun Number.formatBigDecimal() = BigDecimal(this.toDouble()).setScale(2, RoundingMode.DOWN)

fun String.formatBigDecimal() = BigDecimal(this.toDouble()).setScale(2, RoundingMode.DOWN)
package com.example.revolut.data.exchange

import com.example.revolut.domain.ExchangeRepository
import com.example.revolut.utils.BaseConverterTest
import com.example.revolut.utils.RxSchedulersRule
import com.example.revolut.utils.formatBigDecimal
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Rule
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.util.Currency

internal class ExchangeRepositoryImplTest: BaseConverterTest() {

  @Rule
  @JvmField var testSchedulerRule = RxSchedulersRule()

    private val exchangeEndpointsApi: ExchangeEndpoints = mockk(relaxed = true)
    private val repository: ExchangeRepository

    init {
        repository = ExchangeRepositoryImpl(exchangeEndpointsApi)
    }

    @Test
    fun `if repository getExchangeRates method call THEN exchangeEndpointsApi getCurrentExchangeRates`() {
        //Act
        repository.getExchangeRates(CURRENCY, AMOUNT)

        //Assert
        verify { exchangeEndpointsApi.getCurrentExchangeRates(CURRENCY.currencyCode)}

    }

    @Test
    fun `if repository getExchangeRates method call RETURNS coefficients map`() {
        //Assert
        val repositoryResult = mutableMapOf<Currency, BigDecimal>()
        every { exchangeEndpointsApi.getCurrentExchangeRates(CURRENCY.currencyCode)} returns Single.just(eurResponse)

        //Act
        var actualResult = repository.getExchangeRates(CURRENCY, AMOUNT).blockingGet()

        //Assert
        assertEquals(eurTestCoefficientMap, actualResult)
    }

    companion object{
        val CURRENCY = Currency.getInstance("EUR")
        val AMOUNT = 1.formatBigDecimal()
    }

}
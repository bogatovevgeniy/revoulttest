package com.example.revolut.presentation

import com.example.revolut.domain.RealTimeExchangeInteractor
import com.example.revolut.utils.BaseConverterTest
import com.example.revolut.utils.RxSchedulersRule
import com.example.revolut.utils.formatBigDecimal
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.TimeUnit

internal class ConverterViewModelTest : BaseConverterTest() {

    @Rule
    @JvmField
    var testSchedulerRule = RxSchedulersRule()

    @MockK
    var exchangeInteractor: RealTimeExchangeInteractor
    var viewModel: ConverterViewModel
    private val testScheduler = TestScheduler()

    init {
        exchangeInteractor = mockk(relaxed = true)
        every { exchangeInteractor.getExchangeProposals(eurCurrency, amount) } returns Single.just(eurProposalsList)
        viewModel = spyk(ConverterViewModel(exchangeInteractor), recordPrivateCalls = true)
    }

    @Ignore // Requires review
    @Test
    fun `WHEN amount changed THEN RealTimeExchangeInteractor getExchangeRates method is called`() {
        //Arrange


        //Act
        viewModel.requestExchangeProposals()
        viewModel.currencyAmountSubject.onNext(Pair(eurCurrency, amount * 2.formatBigDecimal()))
        testSchedulerRule.getTestScheduler()?.advanceTimeBy(200, TimeUnit.MILLISECONDS);

        //Assert
        verify { exchangeInteractor.getExchangeProposals(eurCurrency, amount) }
    }

    @Ignore // Requires review
    @Test
    fun `WHEN currency changed THEN RealTimeExchangeInteractor getExchangeRates method is called`() {
        //Arrange

        //Act
        viewModel.requestExchangeProposals()
        viewModel.currencyAmountSubject.onNext(Pair(audCurrency, amount))
        testSchedulerRule.getTestScheduler()?.advanceTimeBy(200, TimeUnit.MILLISECONDS);

        //Assert
        verify { exchangeInteractor.getExchangeProposals(eurCurrency, amount) }
    }

    @Ignore // Requires review
    @Test
    fun `WHEN 1 second interval passed THEN RealTimeExchangeInteractor getExchangeRates method is called`() {
        //Arrange

        //Act
        viewModel.requestExchangeProposals()
        testScheduler.advanceTimeBy(1, TimeUnit.MILLISECONDS)
        testSchedulerRule.getTestScheduler()?.advanceTimeBy(1200, TimeUnit.MILLISECONDS);

        //Assert
        verify { exchangeInteractor.getExchangeProposals(eurCurrency, amount) }
    }
}
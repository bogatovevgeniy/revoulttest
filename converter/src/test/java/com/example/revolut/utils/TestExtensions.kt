package com.example.revolut.utils

import com.google.gson.GsonBuilder
import java.io.File
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.net.URLDecoder
import java.nio.charset.Charset

fun <T> loadFromFile(testClass: Class<*>, responseClass: Class<T>?, fileName: String?): T? {
    val resourceUrl = testClass.classLoader!!.getResource(fileName)
    var path = resourceUrl.path
    try {
        path = URLDecoder.decode(resourceUrl.path, "utf-8")
    } catch (e: UnsupportedEncodingException) {
        e.printStackTrace()
    }
    val responseFile = File(path)
    try {
        val jsonResponse: String = responseFile.readText(Charset.forName("utf-8"))
        val gsonBuilder = GsonBuilder()
        return gsonBuilder.create().fromJson(jsonResponse, responseClass)
    } catch (e: IOException) {
        e.printStackTrace()
    }
    return null
}

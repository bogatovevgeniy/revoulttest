package com.example.revolut.utils

import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement


class RxSchedulersRule : TestRule {

    private val testScheduler = TestScheduler()

    fun getTestScheduler(): TestScheduler? {
        return testScheduler
    }

    override fun apply(base: Statement, d: Description?): Statement? {
        return object : Statement() {
            @Throws(Throwable::class)
            override fun evaluate() {
                RxJavaPlugins.setIoSchedulerHandler { scheduler: Scheduler? -> testScheduler }
                RxJavaPlugins.setComputationSchedulerHandler { scheduler: Scheduler? -> testScheduler }
                RxJavaPlugins.setNewThreadSchedulerHandler { scheduler: Scheduler? -> testScheduler }
                RxAndroidPlugins.setMainThreadSchedulerHandler { scheduler: Scheduler? -> Schedulers.trampoline() }
                try {
                    base.evaluate()
                } finally {
                    RxJavaPlugins.reset()
                    RxAndroidPlugins.reset()
                }
            }
        }
    }
}
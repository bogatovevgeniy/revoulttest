package com.example.revolut.utils

import com.example.revolut.data.model.response.CurrencyResponse
import com.example.revolut.domain.model.ExchangeProposal
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import java.math.BigDecimal
import java.util.Currency

open class BaseConverterTest {
    val eurCurrency = Currency.getInstance("EUR")
    val audCurrency = Currency.getInstance("AUD")
    val bgnCurrency = Currency.getInstance("BGN")
    val amount = 1.formatBigDecimal()

    val eurResponse = loadFromFile(
        this::class.java,
        CurrencyResponse::class.java,
        "coefficients_response_EUR.json"
    )
    val audResponse = loadFromFile(
        this::class.java,
        CurrencyResponse::class.java,
        "coefficients_response_AUD.json"
    )
    val audTestCoefficientMap = mutableMapOf<Currency, BigDecimal>().apply {
        put(Currency.getInstance("BGN"), 1.24.formatBigDecimal())
        put(Currency.getInstance("BGN"), 0.638.formatBigDecimal())
    }

    val audProposalsList = mutableListOf(
        ExchangeProposal(audCurrency, amount, true),
        ExchangeProposal(eurCurrency, amount),
        ExchangeProposal(bgnCurrency, amount)
    )

    val eurProposalsList = mutableListOf(
        ExchangeProposal(eurCurrency, amount, true),
        ExchangeProposal(audCurrency, amount),
        ExchangeProposal(bgnCurrency, amount)
    )

    val eurTestCoefficientMap = mutableMapOf<Currency, BigDecimal>().apply {
        put(Currency.getInstance("AUD"), 1.609.formatBigDecimal())
        put(Currency.getInstance("BGN"), 1.992.formatBigDecimal())
    }



    @Before
    fun setUp() {
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
    }

}

package com.example.revolut.domain

import com.example.revolut.data.exchange.ExchangeRepositoryImpl
import com.example.revolut.domain.model.ExchangeProposal
import com.example.revolut.utils.BaseConverterTest
import com.example.revolut.utils.RxSchedulersRule
import com.example.revolut.utils.formatBigDecimal
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import io.reactivex.Single
import org.junit.Rule
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.Currency


internal class RealTimeExchangeInteractorTest : BaseConverterTest() {

    @Rule
    @JvmField var testSchedulerRule = RxSchedulersRule()

    @MockK
    val exchangeRepository: ExchangeRepository = spyk(ExchangeRepositoryImpl(mockk()))
    var exchangeInteractor: RealTimeExchangeInteractor

    init {
        exchangeInteractor = spyk(RealTimeExchangeInteractor(exchangeRepository), recordPrivateCalls = true)
    }

    @Test
    fun `exchange amount was changed to new positive for the same currency and previous calculations exists THEN fastRecalculationMethod is called `() {
        //Arrange
        val exchangeProposal = ExchangeProposal(eurCurrency, amount, true)
        val exchangeProposals = mutableListOf<ExchangeProposal>().apply { add(exchangeProposal) }
        exchangeInteractor.exchangeProposals = exchangeProposals
        every { exchangeRepository.getExchangeRates(eurCurrency, amount) } returns Single.just(eurTestCoefficientMap)

        //Act
        exchangeInteractor.getExchangeProposals(eurCurrency, amount.add(amount))

        //Assert
        verify { exchangeInteractor["fastRecalculationNewAmount"](eurCurrency, amount.add(amount), exchangeProposals) }
    }

    @Test
    fun `exchange amount was changed to zero value for the same currency and previous calculations exists THEN fastRecalculationMethod is called result contains zero proposals`() {
        //Arrange
        val actualResult: MutableList<ExchangeProposal> = mutableListOf()
        val expectedResult: MutableList<ExchangeProposal> = mutableListOf(
            ExchangeProposal(eurCurrency, 0.formatBigDecimal(), true),
            ExchangeProposal(Currency.getInstance("AUD"), 0.formatBigDecimal()),
            ExchangeProposal(Currency.getInstance("BGN"), 0.formatBigDecimal())
        )

        val exchangeProposals = mutableListOf<ExchangeProposal>().apply {
            add(ExchangeProposal(eurCurrency, amount, true))
            add(ExchangeProposal(Currency.getInstance("AUD"), 1.formatBigDecimal()))
            add(ExchangeProposal(Currency.getInstance("BGN"), 2.formatBigDecimal()))
        }
        
        exchangeInteractor.exchangeProposals = exchangeProposals
        every { exchangeRepository.getExchangeRates(eurCurrency, amount) } returns Single.just(eurTestCoefficientMap)

        //Act
        exchangeInteractor.getExchangeProposals(eurCurrency, 0.formatBigDecimal()).subscribe{ list -> actualResult.addAll(list)}

        //Assert
        verify { exchangeInteractor["fastRecalculationNewAmount"](eurCurrency, 0.formatBigDecimal(), exchangeProposals) }
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun `currency was changed THEN repository getExchangeRates method is called `() {
        //Arrange
        every { exchangeRepository.getExchangeRates(eurCurrency, amount) } returns Single.just(eurTestCoefficientMap)

        //Act
        exchangeInteractor.getExchangeProposals(eurCurrency, amount)

        //Assert
        verify { exchangeRepository.getExchangeRates(eurCurrency, amount) }
    }
}
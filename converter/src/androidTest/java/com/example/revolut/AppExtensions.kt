package com.example.revolut

import com.example.revolut.presentation.ConverterActivity
import com.example.revolut.presentation.RevolutApplication

val ConverterActivity.app: RevolutApplication
    get() = application as RevolutApplication
